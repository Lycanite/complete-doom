class Shocktrooper : Actor {
	Default {
		Tag "Shocktrooper";
		
		Health 100;
		Radius 64;
		Height 64;
		Mass 100;
		Speed 10;
		PainChance 30;
        ReactionTime 8;

		DropItem "Cell";

        MONSTER;
		
		SeeSound "Shocktrooper/See";
		PainSound "Shocktrooper/Pain";
		DeathSound "Shocktrooper/Death";
		ActiveSound "Shocktrooper/Active";
		Obituary "%o was killed by a shocktrooper.";
		HitObituary "%o was killed by a shocktrooper.";
	}
	
	States {
		Spawn:
			PPOS AB 10 A_Look();
			Loop;
			
		See:
			PPOS AABBCCDD 3 A_Chase();
			Loop;
			
		Missile:
			PPOS E 10 A_FaceTarget();
			PPOS F 2 fast A_SpawnProjectile("PlasmaBall");
			PPOS E 4 fast;
			PPOS F 2 fast A_SpawnProjectile("PlasmaBall");
			PPOS E 4 fast;
			PPOS F 2 fast A_SpawnProjectile("PlasmaBall");
			PPOS E 4 fast;
			Goto Missile + 1;
			
		Pain:
			PPOS G 3;
			PPOS G 3 A_Pain();
			Goto See;
			
		Death:
			PPOS H 0 A_FaceTarget();
			PPOS H 5 A_SpawnItemEx("ShocktrooperHead", 0.0, 0.0, 40.0, 2.0, 0.0, 1.5, 175.0);
			PPOS I 5 A_Scream();
			PPOS J 5 A_Fall();
			PPOS KL 5;
			PPOS M -1;
			Stop;
			
		XDeath:
			PPOS N 5;
			PPOS O 5 A_XScream();
			PPOS P 5 A_Fall();
			PPOS Q 0 A_FaceTarget();
			PPOS Q 5 A_SpawnItemEx("ShocktrooperTorso", 0.0, -8.0, 32.0, 4.0, 0.0, 2.0, 170.0);
			PPOS RST 5;
			PPOS U -1;
			Stop;
		
		Raise:
			PPOS MLKJIH 5;
			Goto See;
	}
}

class ShocktrooperHead : Actor {
	Default {
		Tag "Shocktrooper Head";

		Damage 0;
		Speed 8;
		Radius 6;
		Height 16;
		Gravity 0.5;
	
		+MISSILE;
		+DROPOFF;
	
		DeathSound "DSPPOHED";
	}

	States {
		Spawn:
			PHED ABCDEFHJI 3;
			Loop;
			
		Death:
			PHED J -1;
			Loop;
	}
}

class ShocktrooperTorso : Actor {
	Default {
		Tag "Shocktrooper Torso";
		
		Damage 0;
		Speed 8;
		Radius 6;
		Height 16;
	
		+NOBLOCKMAP;
		+MISSILE;
		+DROPOFF;
	
		DeathSound "DSPPOHED";
	}

	States {
		Spawn:
			PPOS V -1;
			Loop;

		Death:
			PPOS W 5;
			PPOS X -1;
			Stop;
	}
}