class Vassago : Actor {
	Default {
		Tag "Vassago";
		
		Health 1000;
		Radius 24;
		Height 64;
		Mass 1000;
		Speed 8;
		PainChance 100;
		Damage 5;
        ReactionTime 8;

		DamageFactor "VassagoFlame", 0;
		DamageType "VassagoFlame";

        MONSTER;
		
		SeeSound "Vassago/See";
		PainSound "Vassago/Pain";
		DeathSound "Vassago/Death";
		ActiveSound "Vassago/Active";
		Obituary "%o was cooked by a vassago.";
		HitObituary "%o was cooked by a vassago.";
	}
	
	States {
		Spawn:
			VASS AB 10 A_Look();
			Loop;
			
		See:
			VASS AABBCCDD 3 A_Chase();
			Loop;
		
		Melee:
		Missile:
			VASS E 0 bright A_StartSound("Vassago/Attack");
			VASS E 8 bright A_FaceTarget();
			VASS FG 4 bright A_FaceTarget();
			VASS H 4 bright A_SpawnProjectile("VassagoFlame");
			Goto See;
			
		Pain:
			VASS I 2 bright;
			VASS I 2 bright A_Pain();
			Goto See;
			
		Death:
			VASS J 8 bright;
			VASS K 8 bright A_Scream();
			VASS L 7 bright bright;
			VASS M 6 bright A_Fall();
			VASS NO 6 bright;
			VASS P 7 bright;
			VASS Q -1 bright A_BossDeath();
			Stop;

		Raise:
			VASS P 8;
			VASS ONMLKJ 8;
			Goto See;
	}
}

class VassagoFlame : Actor {
    Default {
        Tag "Vassago Flame";

        Damage 5;
        Speed 15;
        FastSpeed 20;
        Radius 6;
        Height 16;
        RenderStyle "Translucent";
		
		DamageType "VassagoFlame";

        PROJECTILE;

        SeeSound "Vassago/Fire";
        DeathSound "Vassago/Explode";
    }

	action void A_VassagoFlame(bool playSound) {
		if (playSound) {
			A_StartSound("Vassago/Burn03");
		}
		A_Explode(10, 128, XF_NOSPLASH|XF_NOALLIES|XF_CIRCULAR, false, 0, 0, 0, "", "VassagoFlame");
	}

    States {
        Spawn:
            VFLM AB 4 bright;
            Loop;

        Death:
            VFLM C 0 bright {
				A_ChangeFlag("NOGRAVITY", false);
				A_ChangeFlag("NOBLOCKMAP", false);
				return A_Jump(128, "Crackle");
			}
			VFLM C 0 bright A_StartSound("Vassago/Burn01");
            Goto Burn;
		
		Crackle:
			VFLM C 0 bright A_StartSound("Vassago/Burn02");
			Goto Burn;
		
		Burn:
			VFLM C 4 bright A_VassagoFlame(false);
			VFLM D 4 bright;
			VFLM E 4 bright;
			VFLM F 4 bright A_VassagoFlame(true);
			VFLM G 4 bright;
			VFLM H 4 bright;
			VFLM F 4 bright A_VassagoFlame(true);
			VFLM G 4 bright;
			VFLM H 4 bright;
			VFLM F 4 bright A_VassagoFlame(true);
			VFLM G 4 bright;
			VFLM H 4 bright;
			VFLM F 4 bright A_VassagoFlame(true);
			VFLM G 4 bright;
			VFLM H 4 bright;
			VFLM F 4 bright A_VassagoFlame(true);
			VFLM G 4 bright;
			VFLM H 4 bright;
			VFLM F 4 bright A_VassagoFlame(true);
			VFLM G 4 bright;
			VFLM H 4 bright;
			VFLM F 4 bright A_VassagoFlame(true);
			VFLM G 4 bright;
			VFLM H 4 bright;
			VFLM I 4 bright A_VassagoFlame(true);
			VFLM J 4 bright;
			VFLM K 4 bright;
			VFLM L 4 bright A_VassagoFlame(false);
			VFLM MNOPQ 4 bright;
			Stop;
    }
}