class ForgottenSoul : Actor {
	Default {
		Tag "Forgotten Soul";
		
		Health 50;
		Radius 16;
		Height 40;
		Mass 50;
		Speed 12;
		FloatSpeed 12;
		PainChance 128;
		Damage 3;
        ReactionTime 8;
		BloodColor "22 99 22";

        MONSTER;
		+FLOAT;
		+NOGRAVITY;
		
		SeeSound "ForgottenSoul/See";
		PainSound "ForgottenSoul/Pain";
		DeathSound "ForgottenSoul/Death";
		ActiveSound "ForgottenSoul/Active";
		Obituary "%o was spooked by a forgotten soul.";
		HitObituary "%o was spooked by a forgotten soul.";
	}
	
	States {
		Spawn:
			GHUL AB 4 A_Look();
			Loop;
			
		See:
			GHUL AABBCCBB 3 A_Chase();
			Loop;
			
		Missile:
			GHUL DE 4 bright A_FaceTarget();
			GHUL F 4 bright A_SpawnProjectile("ForgottenSoulBall", (invoker.height / 2) - 8.0);
            GHUL G 4 bright;
			Goto See;
			
		Pain:
			GHUL I 3 bright;
			GHUL K 3 bright A_Pain();
			Goto See;
			
		Death:
			GHUL L 5 bright;
			GHUL M 5 A_Scream();
			GHUL NO 5 bright;
			GHUL P 5 A_Fall();
			GHUL QR 5;
			GHUL S -1;
			Stop;
	}
}

class ForgottenSoulBall : Actor {
    Default {
        Tag "Forgotten Soul Ball";
		
        Damage 3;
        Speed 15;
        FastSpeed 20;
        Radius 6;
        Height 8;
        RenderStyle "Translucent";

        PROJECTILE;

        SeeSound "ForgottenSoul/Fire";
        DeathSound "ForgottenSoul/Explode";
    }

    States {
        Spawn:
            GBAL AB 4 bright;
            Loop;
			
        Death:
            GBAL C 5 bright;
            APBX BCDE 5 bright;
            Stop;
    }
}