class Mindweaver : Actor {
	Default {
		Tag "Mindweaver";
		
		Health 500;
		Radius 64;
		Height 64;
		Mass 600;
		Speed 12;
		FloatSpeed 12;
		PainChance 40;
		Damage 3;
        ReactionTime 8;

        MONSTER;
		
		SeeSound "Mindweaver/See";
		PainSound "baby/pain";
		DeathSound "Mindweaver/Death";
		ActiveSound "Mindweaver/Active";
		Obituary "%o let a mindweaver get %h.";
		HitObituary "%o let a mindweaver get %h.";
	}
	
	States {
		Spawn:
			CSPI AB 10 A_Look();
			Loop;
			
		See:
			CSPI A 20;
		Walk:
			CSPI A 0 A_StartSound("Mindweaver/Step");
			CSPI AABBCC 3 A_Chase();
			CSPI D 0 A_StartSound("Mindweaver/Step");
			CSPI DDEEFF 3 A_Chase();
			Loop;
			
		Missile:
			CSPI A 20 bright A_FaceTarget();
			CSPI G 4 bright A_SPosAttack();
            CSPI G 4 bright A_MonsterRefire(10, "See");
			Goto Missile + 1;
			
		Pain:
			CSPI I 3 bright;
			CSPI I 3 bright A_Pain();
			Goto Walk;
			
		Death:
			CSPI J 20 A_Scream();
			CSPI P 7 A_Fall();
			CSPI LMNO 6;
			CSPI P -1 A_BossDeath();
			Stop;
		
		Raise:
			CSPI PONMLKJ 5;
			Goto Walk;
	}
}