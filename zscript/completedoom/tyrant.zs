class Tyrant : Cyberdemon {
	Default {
		Tag "Tyrant";
		
		Health 1000;

		DamageFactor "TyrantRocket", 0;
		DamageType "TyrantRocket";

        +BOSS;
		
		SeeSound "Tyrant/See";
		PainSound "cyber/pain";
		DeathSound "Tyrant/Death";
		ActiveSound "Tyrant/Active";
		Obituary "%o was splattered by a tyrant.";
		HitObituary "%o was splattered by a tyrant.";
	}
	
	States {
		Spawn:
			CYB2 AB 10 A_Look();
			Loop;
			
		See:
			CYB2 A 3 A_Hoof();
			CYB2 ABBCC 3 A_Chase();
			CYB2 D 0 A_StartSound("DSTYRWLK");
			CYB2 DD 3 A_Chase();
			Loop;
		
		Missile:
			CYB2 E 6 A_FaceTarget();
			CYB2 F 12 bright A_CyberAttack();
			CYB2 E 6 A_FaceTarget();
			CYB2 F 12 bright A_CyberAttack();
			CYB2 E 6 A_FaceTarget();
			CYB2 F 12 bright A_CyberAttack();
			Goto See;
			
		Pain:
			CYB2 I 10 A_Pain();
			Goto See;
			
		Death:
			CYB2 H 10;
			CYB2 I 10 A_Scream();
			CYB2 JKL 10;
			CYB2 M 10 A_Fall();
			CYB2 NO 10;
			CYB2 P 30;
			CYB2 P -1 A_BossDeath();
			Stop;
	}
}

class TyrantBoss1 : Tyrant {}
class TyrantBoss2 : Tyrant {}