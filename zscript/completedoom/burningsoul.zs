class BurningSoul : Actor {
	Default {
		Tag "Burning Soul";
		
		Health 100;
		Radius 16;
		Height 40;
		Mass 500;
		Speed 8;
		FloatSpeed 8;
		PainChance 64;
		Damage 3;
        ReactionTime 8;

        MONSTER;
		+FLOAT;
		+NOGRAVITY;
		
		SeeSound "BurningSoul/See";
		PainSound "BurningSoul/Pain";
		DeathSound "BurningSoul/Death";
		Obituary "%o was spooked by a burning soul.";
		HitObituary "%o was spooked by a burning soul.";
	}
	
	States {
		Spawn:
			BSHE AB 4 bright A_Look();
			Loop;
			
		See:
			BSHE AAABBBCCCAAABBBCCC 2 bright A_Chase();
			BSHE A 0 bright A_StartSound("BurningSoul/Active");
			Loop;
			
		Melee:
			BSHE D 1 bright A_Explode(100, 8, XF_CIRCULAR);
			Goto See;
			
		Pain:
			BSHE D 3 bright;
			BSHE D 3 bright A_Pain();
			Goto See;
			
		Death:
			BSHE D 4 bright A_Scream();
			BSHE E 6 bright A_Explode(128, 128, XF_CIRCULAR);
			BSHE F 8 bright A_Fall();
			BSHE G 6 bright;
			BSHE H 4 bright;
			TNT1 A 20;
			Stop;
	}
}